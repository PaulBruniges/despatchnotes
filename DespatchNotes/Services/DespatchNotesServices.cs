﻿using Services.Models;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace Services
{
    public class DespatchNotesServices
    {
        private string dwhConnectionstring;
        private DateTime lastRun;
        private string signatory;
        private string status;
        private string approvalRequired;
        private DateTime cutOffDate;
        private int emailsProcessedOK = 0;
        private int emailsCustomersNotWantingNotification = 0;
        private string customersNotWantingNotifications = string.Empty;
        private int emailsNoEmailAddress = 0;
        private int emailsNotValid = 0;
        private int emailsOrderNotFound = 0;
        private string OrderSuffixNotFound = string.Empty;


        public DespatchNotesServices(string dwConnectionString)
        {
            dwhConnectionstring = dwConnectionString;
        }

        public void RunDespatchNotesJob()
        {
            GetEmailParameters();

            var branchRepository = new BranchRepository();
            var branches = branchRepository.GetBranches(dwhConnectionstring);

            var serviceLevelRepository = new ServiceLevelRepository();
            var services = serviceLevelRepository.GetServiceLevels(dwhConnectionstring);

            var StxConnection = new OdbcConnection("DSN=INFORMIXrout;");
            var StxCommand = new OdbcCommand();

            try
            {

                StxCommand.CommandTimeout = 0;
                StxCommand.Connection = StxConnection;
                StxConnection.Open();

                var consignmentData = GetConsignmentData(StxConnection, StxCommand);

                if (consignmentData.Count == 0)
                {
                    return;
                }

                var helperService = new HelperService();

                foreach (var consignment in consignmentData)
                {

                    if (string.IsNullOrEmpty(consignment.EdiASN))
                    {
                        emailsCustomersNotWantingNotification += 1;
                        customersNotWantingNotifications += "Account: " + consignment.Account + " Order: " + consignment.OrderReference + " Suffix: " + consignment.Suffix + "<br/>";
                    }
                    else
                    {
                        if (consignment.DeliveryEmail != "")
                        {
                            if (helperService.IsValidEmail(consignment.DeliveryEmail))
                            {
                                SendDespatchEmails(consignment, services, branches);
                            }
                            else
                            {
                                emailsNotValid += 1;
                            }
                        }
                        else
                        {
                            emailsNoEmailAddress += 1;
                        }
                    }

                }

                SendDespatchConfirmationToManagers();

                helperService.UpdateLastRunDate(dwhConnectionstring);

            }
            catch
            {
                throw;
            }
            finally
            {
                if (StxCommand != null)
                {
                    StxCommand.Dispose();
                }

                if (StxConnection != null)
                {
                    if (StxConnection.State != System.Data.ConnectionState.Closed)
                    {
                        StxConnection.Close();
                        StxConnection.Dispose();
                    }
                }

            }

        }

       
        public void SendDespatchConfirmationToManagers()
        {
            Email email = new Email();

            email.Subject = "Despatch Email Process - Test Run";

            email.Body = "The Despatch Email Process has been completed.<br/><br/>";

            email.Body += "Delivery Notes Processed OK: " + emailsProcessedOK + ".<br/><br/>";

            email.Body += "Delivery Notes with no Email Address: " + emailsNoEmailAddress + ".<br/><br/>";

            email.Body += "Delivery Notes with Invalid Email Address: " + emailsNotValid + ".<br/><br/>";

            email.Body += "Delivery Notes For customers not wanting Notifications: " + emailsCustomersNotWantingNotification + ".<br/>";
            email.Body += customersNotWantingNotifications;

            email.Body += "<br/>";
            email.Body += "Delivery Notes Where Order/Suffix Was Not Found: " + emailsOrderNotFound + "<br/>";
            email.Body += OrderSuffixNotFound;

            email.Body += "<br/>";
            email.Body += "Total Notes Processed: " + (emailsProcessedOK + emailsNoEmailAddress + emailsNotValid + emailsCustomersNotWantingNotification + emailsOrderNotFound);

            List<string> recipients = new List<string>();
            recipients.Add("paul.bruniges@routeco.com");
            recipients.Add("phil.davis@routeco.com");
            recipients.Add("darren.lack@routeco.com");
            recipients.Add("simon.shenton@routeco.com");
            recipients.Add("mmg@routeco.com");

            email.ToAddresses = recipients;

            var emailRepository = new EmailRepository();
            emailRepository.SendEmail(email);


        }

        public void SendDespatchEmails(ConsigmentData consigment, List<ServiceLevel> services, List<Branch> branches)
        {
            Email email = new Email();

            email.Subject = "Routeco Limited Despatch Notification";
            email.Body = "Hello <br/><br/>";

            email.Body += "We are pleased to confirm that Routeco Limited have despatched the item(s) shown below. <br/><br/>";

            email.Body += "<b>Invoice Address:</b><br/>";

            if (consigment.CompanyName != "")
            {
                email.Body += consigment.CompanyName + "<br/>";
            }

            if (consigment.CompanyAddress1 != "")
            {
                email.Body += consigment.CompanyAddress1 + "<br/>";
            }

            if (consigment.CompanyAddress2 != "")
            {
                email.Body += consigment.CompanyAddress2 + "<br/>";
            }

            if (consigment.CompanyAddress3 != "")
            {
                email.Body += consigment.CompanyAddress3 + "<br/>";
            }

            if (consigment.CompanyAddress4 != "")
            {
                email.Body += consigment.CompanyAddress4 + "<br/>";
            }

            if (consigment.CompanyAddress5 != "")
            {
                email.Body += consigment.CompanyAddress5 + "<br/>";
            }

            if (consigment.CompanyPostcode != "")
            {
                email.Body += consigment.CompanyPostcode + "<br/><br/>";
            }

            email.Body += "<b>Your order is being sent to:</b><br/>";


            if (consigment.DeliveryName != "")
            {
                email.Body += consigment.DeliveryName + "<br/>";
            }

            if (consigment.DeliveryAddress1 != "")
            {
                email.Body += consigment.DeliveryAddress1 + "<br/>";
            }

            if (consigment.DeliveryAddress2 != "")
            {
                email.Body += consigment.DeliveryAddress2 + "<br/>";
            }

            if (consigment.DeliveryAddress3 != "")
            {
                email.Body += consigment.DeliveryAddress3 + "<br/>";
            }

            if (consigment.DeliveryAddress4 != "")
            {
                email.Body += consigment.DeliveryAddress4 + "<br/>";
            }

            if (consigment.DeliveryAddress5 != "")
            {
                email.Body += consigment.DeliveryAddress5 + "<br/>";
            }

            if (consigment.DeliveryPostcode != "")
            {
                email.Body += consigment.DeliveryPostcode + "<br/><br/>";
            }

            email.Body += "Routeco Order reference: " + consigment.OrderReference + "<br/><br/>";

            email.Body += "Your PO Ref: " + consigment.CustomerReference + "<br/><br/>";

            email.Body += "The following items have been despatched: <br/><br/>";

            string itemsTable = "<table><tr><th style='background-color:#B6D54B; border:solid 1px Gray;'>Routeco Order Ref</th><th style='background-color:#B6D54B; border:solid 1px Gray;'>Delivery Note</th><th style='background-color:#B6D54B; border:solid 1px Gray;'>Your PO Ref</th><th style='background-color:#B6D54B; border:solid 1px Gray;'>Product</th><th style='background-color:#B6D54B; border:solid 1px Gray;'>Quantity</th><th style='background-color:#B6D54B; border:solid 1px Gray;'>Description</th></tr>";

            var helperService = new HelperService();

            var orderLines = helperService.GetOrderLines(consigment.OrderReference, consigment.Suffix);

            foreach (var line in orderLines)
            {
                itemsTable += "<tr><td style='border:solid 1px Gray;'>" + consigment.OrderReference + "</td><td style='border:solid 1px Gray;'>" + consigment.Suffix + "</td><td style='border:solid 1px Gray;'>" + consigment.CustomerReference + "</td><td style='border:solid 1px Gray;'>" + line.Product + "</td><td style='border:solid 1px Gray;'>" + line.Quantity + "</td><td style='border:solid 1px Gray;'>" + line.ProductDescription + "</td>";
            }

            itemsTable += "</table><br/><br/>";

            email.Body += itemsTable;

            email.Body += "You can track your delivery by using the link below: " + "<br/><br/><a href='https://www.fedex.com/apps/fedextrack/?tracknumbers=" + consigment.Consignment.Trim() + "'> https://www.fedex.com/apps/fedextrack/?tracknumbers=" + consigment.Consignment.Trim() + "</a><br/><br/>";

            email.Body += "Service provider is " + consigment.Carrier + "<br/><br/>";

            string serviceLevelDescription = string.Empty;

            foreach (var serviceLevel in services)
            {
                if (serviceLevel.Code == consigment.CarSer)
                {
                    serviceLevelDescription = serviceLevel.Description;
                }
            }

            email.Body += "Service level is " + serviceLevelDescription + "<br/><br/>";

            email.Body += "Consignment number is " + consigment.Consignment + "<br/><br/>";

            string branchTel = string.Empty;
            string branchEmail = string.Empty;

            foreach (var branch in branches)
            {
                if (branch.Loc == consigment.OrderStockLocation)
                {
                    branchTel = branch.Telephone;
                    branchEmail = branch.EmailBranch;
                }
            }

            email.Body += "If you have any queries regarding this notification then please contact your local Routeco branch on " + branchTel + " or email <a href='mailto:'" + branchEmail + "'>" + branchEmail + "</a>. <br/><br/>";

            email.Body += "Thank you for your order.";


            List<string> recipients = new List<string>();
            recipients.Add(consigment.DeliveryEmail);

            email.ToAddresses = recipients;

            var emailRepository = new EmailRepository();
            emailRepository.SendEmail(email);

            emailsProcessedOK += 1;
        }


        public List<Models.ConsigmentData> GetConsignmentData(OdbcConnection stxConnection, OdbcCommand stxCommand)
        {
            var ConsignmentDataList = new List<Models.ConsigmentData>();
            var helperService = new HelperService();

            var commandString = "select Consignment, Suffix, Orderref, cast(concat(FORMAT(date,'yyyy-MM-dd '),FORMAT(time,'HH:mm:ss')) as datetime) as ManifestDate,Carrier ";
            commandString += "from carrier_done where RecordType=1 and not suffix is null and ";
            commandString += "cast(concat(FORMAT(date,'yyyy-MM-dd '),FORMAT(date,'HH:mm:ss')) as datetime) >= cast('" + lastRun.ToString("yyyy-MM-dd 00:00:00") + "' as datetime) AND ";
            commandString += "cast(concat(fORMAT(date,'yyyy-MM-dd '),FORMAT(date,'HH:mm:ss')) as datetime) < cast('" + cutOffDate.ToString("yyyy-MM-dd HH:mm:ss") + "' as datetime) ";
            commandString += "AND Carrier = 'Fedex' AND OrderRef NOT LIKE 'SU%'";

            List<Models.ServiceLevel> serviceLevels = new List<Models.ServiceLevel>();

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(commandString, connection);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {

                    var orderHeader = helperService.GetSalesOrderHeader(reader["OrderRef"].ToString(), (int)reader["Suffix"]);

                    if (!(orderHeader is null))
                    {

                        var consigmentData = new ConsigmentData()
                        {
                            Consignment = reader["Consignment"].ToString(),
                            Suffix = (int)reader["Suffix"],
                            OrderReference = reader["OrderRef"].ToString(),
                            ManifestDate = (DateTime)reader["ManifestDate"],
                            Carrier = reader["Carrier"].ToString(),
                            Account = orderHeader.Account,
                            DeliveryEmail = orderHeader.DeliveryEmail,
                            DeliveryName = orderHeader.DeliveryName,
                            DeliveryAddress1 = orderHeader.DeliveryAddress1,
                            DeliveryAddress2 = orderHeader.DeliveryAddress2,
                            DeliveryAddress3 = orderHeader.DeliveryAddress3,
                            DeliveryAddress4 = orderHeader.DeliveryAddress4,
                            DeliveryAddress5 = orderHeader.DeliveryAddress5,
                            DeliveryPostcode = orderHeader.DeliveryPostcode,
                            CompanyName = orderHeader.CompanyName,
                            CompanyAddress1 = orderHeader.CompanyAddress1,
                            CompanyAddress2 = orderHeader.CompanyAddress2,
                            CompanyAddress3 = orderHeader.CompanyAddress3,
                            CompanyAddress4 = orderHeader.CompanyAddress4,
                            CompanyAddress5 = orderHeader.CompanyAddress5,
                            CompanyPostcode = orderHeader.CompanyPostcode,
                            CustomerReference = orderHeader.CustomerReference,
                            DeliveryDescriptio = orderHeader.DeliveryDescription,
                            OrderStockLocation = orderHeader.StockLocation,
                            CarSer = orderHeader.CarSer,
                            SODCarrier = orderHeader.Carrier,
                            EdiASN = orderHeader.EDIASN
                        };

                        ConsignmentDataList.Add(consigmentData);
                    }
                    else
                    {
                        emailsOrderNotFound += 1;
                        OrderSuffixNotFound += reader["OrderRef"].ToString() + " Suffix: " + reader["Suffix"].ToString() + "<br/>";

                    }

                }


                connection.Close();
            }

            return ConsignmentDataList;

        }

        public void GetEmailParameters()
        {
            string sql = "Select txtEmailSignatory, txtStatus, txtApprovalRequired, dtmLastRun, dtmCutOffDate from tblemailparameters where intEmailID=4";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();
                var reader = command.ExecuteReader();

                if (reader.Read())
                {
                    if (reader["txtStatus"].ToString() != "A")
                    {
                        return;
                    }

                    lastRun = Convert.ToDateTime(reader["dtmLastRun"]);
                    signatory = reader["txtEmailSignatory"].ToString();
                    approvalRequired = reader["txtApprovalRequired"].ToString();
                    cutOffDate = Convert.ToDateTime(reader["dtmCutOffDate"]);
                }
                else
                {
                    return;
                }


                connection.Close();
            }


        }



    }
}
