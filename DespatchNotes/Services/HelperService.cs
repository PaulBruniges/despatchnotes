﻿
using Services.Models;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Services
{
    public class HelperService
    {

        public bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public string getAccount(string orderRef)
        {
            string sql = "select DISTINCT soh_account from sohead where soh_ordref='" + orderRef + "'";
            string account = string.Empty;

            using (var connection = new OdbcConnection("DSN=INFORMIXrout;"))
            {
                connection.Open();
                var command = new OdbcCommand(sql, connection);
                account = command.ExecuteScalar().ToString().Trim();

                connection.Close();
            }

            return account;
        }


        public void UpdateLastRunDate(string dwhConnectionString)
        {
            string sql = "Update tblemailparameters set dtmLastRun=GETDATE() where intEmailID=4";


            using (var connection = new SqlConnection(dwhConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }

        }


        public OrderHeader GetSalesOrderHeader(string orderRef, int suffix)
        {
            string commandString = "select soh_account, soh_userid, soh_stloc, soh_delemail, soh_ediasn, soh_cusref, sod_delname, sod_delad1, sod_delad2, sod_delad3, sod_delad4, sod_delad5, sod_delpost, ";
            commandString += "ndm_name, ndm_addr1, ndm_addr2, ndm_addr3, ndm_addr4, ndm_addr5, ndm_postcode, dlcus_ediasn, sodl_deldesc, sod_carrier, sod_carser ";
            commandString += "from sohead, sodeliv, ndmas, dlcust, outer(sodels) ";
            commandString += "where soh_account=sod_account and soh_account=dlcus_customer and soh_ordref=sod_ordref and soh_account=ndm_ndcode and soh_trcode=sodl_trcode and soh_area=sodl_area and soh_serlev = sodl_serlev and sodl_vanarea='ROUT' and ";
            commandString += "soh_ordref='" + orderRef + "' and sod_suffix=" + suffix;

            using (var connection = new OdbcConnection("DSN=INFORMIXrout;"))
            {
                connection.Open();
                var command = new OdbcCommand(commandString, connection);
                var reader = command.ExecuteReader();

                if (reader.Read())
                {
                    var orderHeader = new OrderHeader()
                    {
                        Account = reader["soh_account"].ToString().Trim(),
                        UserID = reader["soh_userid"].ToString().Trim(),
                        StockLocation = reader["soh_stLoc"].ToString().Trim(),
                        DeliveryEmail = reader["soh_delemail"].ToString().Trim(),
                        EDIASN = reader["soh_ediasn"].ToString().Trim(),
                        CustomerReference = reader["soh_cusref"].ToString().Trim(),
                        DeliveryName = reader["sod_delname"].ToString().Trim(),
                        DeliveryAddress1 = reader["sod_delad1"].ToString().Trim(),
                        DeliveryAddress2 = reader["sod_delad2"].ToString().Trim(),
                        DeliveryAddress3 = reader["sod_delad3"].ToString().Trim(),
                        DeliveryAddress4 = reader["sod_delad4"].ToString().Trim(),
                        DeliveryAddress5 = reader["sod_delad5"].ToString().Trim(),
                        DeliveryPostcode = reader["sod_delpost"].ToString().Trim(),
                        CompanyName = reader["ndm_name"].ToString().Trim(),
                        CompanyAddress1 = reader["ndm_addr1"].ToString().Trim(),
                        CompanyAddress2 = reader["ndm_addr2"].ToString().Trim(),
                        CompanyAddress3 = reader["ndm_addr3"].ToString().Trim(),
                        CompanyAddress4 = reader["ndm_addr4"].ToString().Trim(),
                        CompanyAddress5 = reader["ndm_addr5"].ToString().Trim(),
                        CompanyPostcode = reader["ndm_postcode"].ToString().Trim(),
                        CustomerEDIASN = reader["dlcus_ediasn"].ToString().Trim(),
                        DeliveryDescription = reader["sodl_deldesc"].ToString().Trim(),
                        Carrier = reader["sod_carrier"].ToString().Trim(),
                        CarSer = reader["sod_carser"].ToString().Trim()
                    };

                    return orderHeader;
                }


                connection.Close();
            }

            return null;
        }


        public List<OrderLine> GetOrderLines(string orderRef, int suffix)
        {
            var orderLines = new List<OrderLine>();

            string commandString = "select soh_cusref, soi_product,soi_itqty,cmp_desc,soi_seq,soi_custprod from sohead,soitem,cmprod ";
            commandString += "where soh_ordref=soi_ordref and soi_product=cmp_product and cmp_desc not like '%Delivery%' and ";       
            commandString += "soh_ordref='" + orderRef + "' and soi_suffix=" + suffix + " order by soi_seq ";

            using (var connection = new OdbcConnection("DSN=INFORMIXrout;"))
            {
                connection.Open();
                var command = new OdbcCommand(commandString, connection);
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var orderLine = new OrderLine()
                    {
                        CustomerReference = reader["soh_cusref"].ToString(),
                        Product = reader["soi_product"].ToString(),
                        Quantity = Convert.ToInt32(reader["soi_itqty"].ToString()),
                        ProductDescription = reader["cmp_desc"].ToString(),
                        Seq = reader["soi_seq"].ToString(),
                        Custprod = reader["soi_custprod"].ToString()
                    };

                    orderLines.Add(orderLine);

                }


                connection.Close();
            }

            return orderLines;

        }
        


    }
}
