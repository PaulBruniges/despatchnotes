﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class JobManagementRepository
    {

        private string dwhConnectionstring;

        public JobManagementRepository(string dwConnectionString)
        {
            dwhConnectionstring = dwConnectionString;
        }


        public void UpdateStartTime(string jobName)
        {
            string sql = "Update tbljobmanagement set prev_last_start_datetime=last_start_datetime, last_start_datetime=GETDATE(), last_end_datetime=null where job_name = '" + jobName + "'";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }

        }

        public void UpdateEndTime(string jobName)
        {
            string sql = "Update tbljobmanagement set last_end_datetime=GETDATE(), prev_last_start_datetime = last_start_datetime, last_successful_run_date = GETDATE() where job_name = '" + jobName + "'";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }
        }


    }
}
