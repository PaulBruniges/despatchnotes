﻿using Services.Models;
using System.Configuration;
using System.Net.Mail;

namespace Services
{
    public class EmailRepository
    {
        public void SendEmail(Email email)
        {
            MailMessage mail = new MailMessage("no-reply@routeco.com", email.ToAddresses[0]);

            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("no-reply@routeco.com", "QVh52XhemW5T6fKdww2R");

            SmtpClient client = new SmtpClient
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = "smtp.office365.com",
                EnableSsl = true,
                Credentials = credentials
            };

            int count = 0;

            foreach (var toAddress in email.ToAddresses)
            {
                if(count >= 1)
                {
                    if (toAddress != "")
                    {
                        mail.To.Add(toAddress);
                    }
                }

                count += 1;
            }

            mail.Bcc.Add("paul.bruniges@routeco.com");
            mail.IsBodyHtml = true;
            mail.Body = email.Body;
            mail.Subject = email.Subject;

            client.Send(mail);
        }

    }
}
