﻿namespace Services.Models
{
    public class OrderLine
    {
        public string CustomerReference { get; set; }

        public string Product { get; set; }

        public int Quantity { get; set; }

        public string ProductDescription { get; set; }

        public string Seq { get; set; }

        public string Custprod { get; set; }
    }
}
