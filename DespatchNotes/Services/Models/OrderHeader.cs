﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
    public class OrderHeader
    {
        public string Account { get; set; }

        public string UserID { get; set; }

        public string StockLocation { get; set; }

        public string DeliveryEmail { get; set; }

        public string EDIASN { get; set; }

        public string CustomerReference { get; set; }

        public string DeliveryName { get; set; }

        public string DeliveryAddress1 { get; set; }

        public string DeliveryAddress2 { get; set; }

        public string DeliveryAddress3 { get; set; }

        public string DeliveryAddress4 { get; set; }

        public string DeliveryAddress5 { get; set; }

        public string DeliveryPostcode { get; set; }

        public string CompanyName { get; set; }

        public string CompanyAddress1 { get; set; }

        public string CompanyAddress2 { get; set; }

        public string CompanyAddress3 { get; set; }

        public string CompanyAddress4 { get; set; }

        public string CompanyAddress5 { get; set; }

        public string CompanyPostcode { get; set; }

        public string CustomerEDIASN { get; set; }

        public string DeliveryDescription { get; set; }

        public string Carrier { get; set; }

        public string CarSer { get; set; }
    }
}
