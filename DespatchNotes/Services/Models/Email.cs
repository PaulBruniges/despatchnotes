﻿using System.Collections.Generic;

namespace Services.Models
{
    public class Email
    {
        public List<string> ToAddresses { get; set; }

        public string FromAddress { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }
    }
}
