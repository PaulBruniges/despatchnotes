﻿using Services;
using System.Configuration;

namespace DespatchNotes
{
    class Program
    {
        static void Main(string[] args)
        {

            JobManagementRepository jobManagement = new JobManagementRepository(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString);
            DespatchNotesServices despatchNotesServices = new DespatchNotesServices(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString);

            jobManagement.UpdateStartTime("UK_DespatchNotes");

            despatchNotesServices.RunDespatchNotesJob();

            jobManagement.UpdateEndTime("UK_DespatchNotes");

        }
    }
}
