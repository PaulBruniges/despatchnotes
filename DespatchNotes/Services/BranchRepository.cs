﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Services
{
    public class BranchRepository
    {
        public List<Models.Branch> GetBranches(string dwhConnectionstring)
        {
            List<Models.Branch> branches = new List<Models.Branch>();
            string commandString = "select txtLoc, txtEmailBranch, txtTelBranch from branch_codes";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(commandString, connection);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var branch = new Models.Branch()
                    {
                        Loc = reader["txtLoc"].ToString(),
                        EmailBranch = reader["txtEmailBranch"].ToString(),
                        Telephone = reader["txtTelBranch"].ToString()
                    };

                    branches.Add(branch);
                }

                connection.Close();
            }

            return branches;
        }
    }
}
