﻿namespace Services.Models
{
    public class ServiceLevel
    {
        public string Code { get; set; }

        public string  Description { get; set; }
    }
}
