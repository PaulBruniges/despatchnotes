﻿using Services.Models;
using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;

namespace Services
{
    public class EndOfDayServices
    {

        private string dwhConnectionstring;

        public string toLoc { get; set; }

        public string PickLoc { get; set; }

        public EndOfDayServices(string dwConnectionString)
        {
            dwhConnectionstring = dwConnectionString;
        }

        public void SetEndOfDay()
        {

            DirectoryInfo dirExport = null;
            FileInfo[] dirFiles = null;

            var mdtLastRunDate = GetEmailParameters();
          
            var strSQL = "";

            var Conn = new SqlConnection(dwhConnectionstring);
            var Cmd = new SqlCommand(strSQL, Conn);
            Conn.Open();

            dirExport = new DirectoryInfo(@"\\10.1.1.100\carriers\uk\fedex\import");

            dirFiles = dirExport.GetFiles();


            foreach (var fi in dirFiles)
            {
                if (fi.Extension.ToString().ToLower() == ".csv")
                {
                    LoadCSVFile("\\\\10.1.1.100\\carriers\\uk\\fedex\\import\\" + fi.Name, fi.LastWriteTimeUtc);

                    fi.MoveTo("\\\\10.1.1.100\\carriers\\uk\\fedex\\import\\done\\" + fi.Name);

                }
            }

            strSQL = "select * from carrier_consignment";
            Cmd.CommandText = strSQL;
            SqlDataReader rdr = Cmd.ExecuteReader();

            while (rdr.Read())
            {
      
                if (rdr["OrderRef"].ToString().StartsWith("SU"))
                {
                    getToLoc(rdr["OrderRef"].ToString());
                    UpdateStockReqConsignmentNumber(rdr["Consignment"].ToString(), rdr["OrderRef"].ToString(), rdr["Suffix"].ToString());
                }
                else
                {
                    var helperService = new HelperService();
                    string account = helperService.getAccount(rdr["OrderRef"].ToString());
                    UpdateOrderConsignmentNumber(rdr["Consignment"].ToString(), account, rdr["OrderRef"].ToString(), rdr["Suffix"].ToString());
                }

                       
            }
           
            InsertintoCarrierDone();

            TruncateCarrierConsignment();

            var cutOffDate = DateTime.Now;

            //Calculate number of despatch emails
            var totalLines = GetTotalNumberOfLines(mdtLastRunDate, cutOffDate);

            var totalConsignments = GetNumberOfConsignments(mdtLastRunDate, cutOffDate);

            //Send emails

            var email = new Email();
            email.Body = "End of Day Despatch Process Notification";

            email.Body = "The End of Day despatch notification process has been completed. <br/><br/>" +
                "The next despatch email process will run for the period " + mdtLastRunDate.ToString("dd/MM/yyyy HH:mm:ss") + " through " + cutOffDate.ToString("dd/MM/yyyy HH:mm:ss") + ".<br/><br/>" +
                "The number of sales order consigments in this period was " + totalConsignments + " covering " + totalLines + " delivery notes.";

            List<string> ToAddresses = new List<string>();
            ToAddresses.Add("paul.bruniges@routeco.com");
            ToAddresses.Add("warehouseadmin@routeco.com");
            ToAddresses.Add("darren.lack@routeco.com");
            ToAddresses.Add("mmg@routeco.com");
            ToAddresses.Add("phil.davis@routeco.com");
            ToAddresses.Add("jason.stamp@routeco.com");
            ToAddresses.Add("simon.shenton@routeco.com");
            ToAddresses.Add("charlie.nash@routeco.com");

            email.ToAddresses = ToAddresses;

            var emailRepository = new EmailRepository();
            emailRepository.SendEmail(email);

            UpdateEmailParameters(cutOffDate);
        }


        public void UpdateEmailParameters(DateTime cutoffdate)
        {
            string sql = "update tblemailparameters set dtmCutOffDate='" + cutoffdate.ToString("yyyy-MM-dd HH:mm:ss") + "' where intEmailID=4";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }
        }


        public int GetTotalNumberOfLines(DateTime lastRunDate, DateTime newCutoffDate)
        {
            var count = 0;
            string sql = "select count(*) as TotalLines from carrier_done Where RecordType=1 and not suffix is null and date >= '" + lastRunDate.ToString("yyyy-MM-dd") + "' and date < '" + newCutoffDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                count = Convert.ToInt32(command.ExecuteScalar());

                connection.Close();
            }

            return count;
        }



        public int GetNumberOfConsignments(DateTime lastRunDate, DateTime newCutoffDate)
        {
            var count = 0;
            string sql = "select count(distinct Consignment) as TotalLines from carrier_done Where RecordType=1 and not suffix is null and date >= '" + lastRunDate.ToString("yyyy-MM-dd") + "' and date < '" + newCutoffDate.ToString("yyyy-MM-dd HH:mm:ss") + "'";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                count = Convert.ToInt32(command.ExecuteScalar());

                connection.Close();
            }


            return count;
        }

        public void InsertintoCarrierDone()
        {
            string sql = "insert into carrier_done select * from carrier_consignment where recordtype is not null and consignment is not null and shipref is not null and suffix is not null and orderref is not null";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public void TruncateCarrierConsignment()
        {
            string sql = "truncate table carrier_consignment";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }
        }



        public void UpdateOrderConsignmentNumber(string consignment, string account, string orderRef, string suffix)
        {
            string sql = "Update sodeliv set sod_receiver='" + consignment + "' where sod_account = '" + account + "' and sod_ordref = '" + orderRef + "' and sod_suffix = " + suffix;

            using (var connection = new OdbcConnection("DSN=INFORMIXrout;"))
            {
                connection.Open();
                var command = new OdbcCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }
        }


        public void UpdateStockReqConsignmentNumber(string consignment, string stockReqRef, string suffix)
        {
            string sql = "Update stdeliv set stdel_receiver='" + consignment + "' where stdel_pikloc = '" + PickLoc + "' and stdel_account = '" + toLoc + "' and stdel_ordref = '" + stockReqRef + "' and stdel_suffix = " + suffix;

            using (var connection = new OdbcConnection("DSN=INFORMIXrout;"))
            {
                connection.Open();
                var command = new OdbcCommand(sql, connection);
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        public void getToLoc(string reqRef)
        {
            string sql = "select stit_loc, stit_toLoc from stitem where stit_ref ='" + reqRef + "'";
            
            using (var connection = new OdbcConnection("DSN=INFORMIXrout;"))
            {
                connection.Open();
                var command = new OdbcCommand(sql, connection);
                var rdr = command.ExecuteReader();

                while (rdr.Read())
                {
                    toLoc = rdr["stit_toLoc"].ToString();
                    PickLoc = rdr["stit_loc"].ToString();
                }
            }

        }



        public void LoadCSVFile(string strFileName, DateTime consignmentCreated)
        {
            string[] values = null;

            using (StreamReader reader = new StreamReader(strFileName))
            {
                string line = null;
                while (null != (line = reader.ReadLine()))
                {
                    values = line.Split(',');
                }


                var commandString = "insert into carrier_consignment(RecordType,Consignment,ShipRef,Suffix,OrderRef,PrintedDate,PrintedTime, Carrier) values(" +
                                    "'1'," +
                                    "'" + values[1] + "'," +
                                    "'" + values[2] + "'," +
                                    values[2].Substring(8) + "," +
                                    "'" + values[2].Substring(0, 8) + "'," +
                                    "'" + consignmentCreated.ToString("yyyy-MM-dd") + "'," +
                                    "'1899/12/30 00:00:00','FEDEX')";

                using (var connection = new SqlConnection(dwhConnectionstring))
                {
                    connection.Open();
                    var command = new SqlCommand(commandString, connection);
                    command.ExecuteNonQuery();

                    connection.Close();

                }
            }
        }


        public DateTime GetEmailParameters()
        {
            var mdtLastRunDate = DateTime.Now;

            string sql = "SELECT dtmLastRun FROM tblemailparameters where intEmailID=4";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(sql, connection);
                mdtLastRunDate = Convert.ToDateTime(command.ExecuteScalar());
            }


            return mdtLastRunDate;
        }

    }
}
