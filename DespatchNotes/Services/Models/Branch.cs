﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Models
{
    public class Branch
    {
        public string Loc { get; set; }

        public string EmailBranch { get; set; }

        public string Telephone { get; set; }
    }
}
