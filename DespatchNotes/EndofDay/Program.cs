﻿using Services;
using System.Configuration;

namespace EndofDay
{
    class Program
    {
        static void Main(string[] args)
        {
            EndOfDayServices despatchServices = new EndOfDayServices(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString);
            JobManagementRepository jobManagement = new JobManagementRepository(ConfigurationManager.ConnectionStrings["SQLDWH"].ConnectionString);

            jobManagement.UpdateStartTime("mySQL_EndofDay");

            despatchServices.SetEndOfDay();

            jobManagement.UpdateEndTime("mySQL_EndofDay");

        }
    }
}
