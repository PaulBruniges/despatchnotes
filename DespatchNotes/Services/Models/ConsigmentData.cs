﻿using System;

namespace Services.Models
{
    public class ConsigmentData
    {
        public string Consignment { get; set; }

        public int Suffix { get; set; }

        public string OrderReference { get; set; }

        public DateTime ManifestDate { get; set; }

        public string Account { get; set; }

        public string DeliveryEmail { get; set; }

        public string DeliveryName { get; set; }

        public string DeliveryAddress1 { get; set; }

        public string DeliveryAddress2 { get; set; }

        public string DeliveryAddress3 { get; set; }

        public string DeliveryAddress4 { get; set; }

        public string DeliveryAddress5 { get; set; }

        public string DeliveryPostcode { get; set; }

        public string CompanyName { get; set; }

        public string CompanyAddress1 { get; set; }

        public string CompanyAddress2 { get; set; }

        public string CompanyAddress3 { get; set; }

        public string CompanyAddress4 { get; set; }

        public string CompanyAddress5 { get; set; }

        public string CompanyPostcode { get; set; }

        public string CustomerReference { get; set; }

        public string OrderStockLocation { get; set; }

        public string DeliveryDescriptio { get; set; }

        public string SODCarrier { get; set; }

        public string CarSer { get; set; }

        public string Carrier { get; set; }

        public string EdiASN { get; set; }
    }
}
