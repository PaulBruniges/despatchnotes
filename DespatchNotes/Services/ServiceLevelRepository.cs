﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace Services
{
    class ServiceLevelRepository
    {
        public List<Models.ServiceLevel> GetServiceLevels(string dwhConnectionstring)
        {
            List<Models.ServiceLevel> serviceLevels = new List<Models.ServiceLevel>();
            string commandString = "select code, descr from serlev";

            using (var connection = new SqlConnection(dwhConnectionstring))
            {
                connection.Open();
                var command = new SqlCommand(commandString, connection);

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var serviceLevel = new Models.ServiceLevel()
                    {
                        Code = reader["code"].ToString().Trim(),
                        Description = reader["descr"].ToString().Trim()
                    };

                    serviceLevels.Add(serviceLevel);
                }

                connection.Close();
            }

            return serviceLevels;
        }
    }
}
